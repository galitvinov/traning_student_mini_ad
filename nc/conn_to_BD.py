#from curses.ascii import isdigit
import mysql.connector
import string
import random

def generate_random_string(length):
    characters = string.ascii_lowercase + string.digits
    rand_string = ''.join(random.choice(characters) for i in range(length))
    return rand_string


def login (logi, pas):
  mydb = mysql.connector.connect(
    host = "scoredb",
    user='connecter',
    password='connecter_pass',
    database='MDB'
  )
  cursor = mydb.cursor()
  cursor.execute("SELECT COUNT(*) FROM User WHERE login='{}';".format(str(logi)))
  login = cursor.fetchone()[0]
  if login:
    cursor.execute("SELECT COUNT(*) FROM User WHERE login='{}' AND pass='{}';".format(str(logi), str(pas)))
    auth = cursor.fetchone()[0]
    if auth:
      cursor.execute("SELECT auth FROM User WHERE login='{}' AND pass='{}';".format(str(logi), str(pas)))
      auth = cursor.fetchone()[0]
      return("Login already exists!\nYour auth:"+auth)
    else:
      return("Login already exists!")
    
  
  auth=generate_random_string(32)
  cursor.execute("INSERT INTO User (login,pass,auth) VALUES ('{}','{}','{}');".format(logi, pas, auth))
  mydb.commit()
  return("Your auth:"+auth)

def ac_flag_lp (logi, pas, flag):
  mydb = mysql.connector.connect(
    host = "scoredb",
    user='connecter',
    password='connecter_pass',
    database='MDB'
  )
  cursor = mydb.cursor()
  cursor.execute("SELECT id FROM User WHERE login='{}' AND pass='{}';".format(str(logi), str(pas)))
  login_r = cursor.fetchone()
  #print(login_r, flush=True)
  if login_r is None:
    return("Authorization failed!")
  login_r = login_r[0]

  cursor.execute("SELECT id FROM Flags WHERE flag='{}';".format(str(flag)))
  flag_r = cursor.fetchone()
  #print(flag_r, flush=True)
  if flag_r is None:
    return("Flag does not exist!")
  flag_r = flag_r[0]

  cursor.execute("SELECT id, time FROM Send_flags WHERE id_usr={} AND id_flag={};".format(login_r, flag_r))
  find_flag = cursor.fetchone()
  #print(find_flag, flush=True)
  if find_flag is not None:
    return("This flag has already been surrendered!")

  cursor.execute("INSERT INTO Send_flags (id_usr, id_flag, time) VALUES ({},{},NOW());".format(login_r, flag_r))
  mydb.commit()
  return("Flag accepted!")



def ac_flag_a (auth, flag):
  mydb = mysql.connector.connect(
    host = "scoredb",
    user='connecter',
    password='connecter_pass',
    database='MDB'
  )
  cursor = mydb.cursor()
  cursor.execute("SELECT id FROM User WHERE auth='{}';".format(str(auth)))
  login_r = cursor.fetchone()
  #print(login_r, flush=True)
  if login_r is None:
    return("Authorization failed!")
  login_r = login_r[0]

  cursor.execute("SELECT id FROM Flags WHERE flag='{}';".format(str(flag)))
  flag_r = cursor.fetchone()
  #print(flag_r, flush=True)
  if flag_r is None:
    return("Flag does not exist!")
  flag_r = flag_r[0]

  cursor.execute("SELECT id, time FROM Send_flags WHERE id_usr={} AND id_flag={};".format(login_r, flag_r))
  find_flag = cursor.fetchone()
  #print(find_flag, flush=True)
  if find_flag is not None:
    return("This flag has already been surrendered!")

  cursor.execute("INSERT INTO Send_flags (id_usr, id_flag, time) VALUES ({},{},NOW());".format(login_r, flag_r))
  mydb.commit()
  return("Flag accepted!")

