import socket
import multiprocessing
import conn_to_BD
import time


def sendget(data, addr):
    data=data.encode('utf-8')
    sock.sendto(data,addr)
    data = sock.recv(1024)
    return data.decode('utf-8').strip()

def handle_connection(sock, addr, stime):
    print('Connected by',addr)
    p_state=0
    with sock:
        while True:
            if p_state==0:
                ret=sendget("Main menu\nChoose your action\n1)Regestation\n2)Accept flag\n3)Exit\nChoice:",addr)
                if ret=='1' or ret=='2':
                    p_state=int(ret)
                elif ret=='3':
                    break
                else:
                    p_state=0
            elif p_state==1:
                log=sendget("Write login:",addr)
                pas=sendget("Write password:",addr)
                ret=conn_to_BD.login(log, pas)
                sendget(ret+"\nPress Enter...",addr)
                p_state=0
            elif p_state==2:
                ret=sendget("Choose your method of surrendering the flag\n1)login and password\n2)auth token\n3)Back\nChoice:",addr)
                if ret=='1' or ret=='2':
                    p_state=int(ret)+20
                elif ret=='3':
                    p_state=0
                else:
                    p_state=2
            elif p_state==21:
                log=sendget("Write login:",addr)
                pas=sendget("Write password:",addr)
                ret="Flag accepted!"
                while ret == "Flag accepted!":
                    flag=sendget("Write flag:",addr)
                    ret=conn_to_BD.ac_flag_lp(log, pas, flag)
                    sendget(ret+"\nPress Enter...",addr)
                p_state=0
            elif p_state==22:
                auth=sendget("Write auth:",addr)
                ret="Flag accepted!"
                while ret == "Flag accepted!":
                    flag=sendget("Write flag:",addr)
                    ret=conn_to_BD.ac_flag_a(auth, flag)
                    sendget(ret+"\nPress Enter...",addr)
                p_state=0
        sendget("Disconnected", addr)


if __name__ == "__main__":
    stime=time.time()
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as serv_sock:
        serv_sock.bind(("", 5000))
        serv_sock.listen(30)
        while True:
            print("Waiting for connection...")
            sock, addr = serv_sock.accept()
            p = multiprocessing.Process(target=handle_connection, args=(sock, addr, stime))
            p.start()

