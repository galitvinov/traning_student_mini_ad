import requests
from client.api_urls import get_url

from methods.result import (
    Result,
    SUCCESS,
    MUMBLE,
    DOWN,
    ERROR,
)
from settings import (
    DEBUG,
)

def health_check(host):
    # check some service endpoints to make sure it works correctly
    s = requests.Session()
    url = get_url(host, 'home')
    r = s.get(url)
    print(r.status_code)
    if 200 == r.status_code:
        return Result(SUCCESS, "It's alive!")
    else:
        return Result(MUMBLE, 'Server does not return 200')
