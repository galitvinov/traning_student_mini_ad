import sys
from sys import exc_info
import requests.utils
import requests
from client import client
import settings
from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from utils import (
    getCred,
    traceback_to_string,
    RestException,
)


def get_flag(host, uid, flag, vuln):
    # check that flag exists in certain vulnerability
    try:
        with requests.Session() as s:
            if int(vuln) == settings.PHP_VULN:
                return get_name_flag(s, host, uid, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, "Wrong command", "Invalid vuln: " + str(vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, 'Service is down', str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, 'Unknown error', traceback_to_string())


def get_name_flag(session, host, uid, flag):
    login, password = getCred(flag)
    print(login, password, host)
    client.login(session, login, password, host)
    content = client.get_name(session, host, flag)

    if flag in content:
        return Result(SUCCESS, 'Flag found')
    else:    
        return Result(CORRUPT, 'Flag not found')

