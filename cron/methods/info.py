from settings import PHP_VULN
from methods.result import (
    Result,
    SUCCESS,
)

def info():
    return Result(SUCCESS, extra_data={
        'sequence': [PHP_VULN],
        'version': '1.0',
    })
