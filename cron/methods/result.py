import sys

ERROR = 110
SUCCESS = 101
CORRUPT = 102
MUMBLE = 103
DOWN = 104

def close(code, public="", private=""):
    if public:
        print(public)
    if private:
        print(private, file=sys.stderr)
    print('Exit with code {}'.format(code), file=sys.stderr)
    exit(code)

class Result:
    def __init__(self, status, message='', debug='', extra_data=None):
        self.status = status
        self.message = message
        self.debug = debug
        self.extra_data = extra_data
