import sys
from sys import exc_info
import requests.utils
import requests
from client import client
import settings
from utils import (
    RestException,
    traceback_to_string,
    getCred,
)
from methods.result import (
    Result,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    ERROR,
    DOWN,
)
from faker import Faker
from hashlib import md5
import random

def put_flag(host, flag, vuln):
    vuln = int(vuln)
    if vuln not in [settings.PHP_VULN]:
        print("Invalid vuln", file=sys.stderr)
        return Result(ERROR, "Invalid vuln")
    try:
        with requests.Session() as s:
            if vuln == settings.PHP_VULN:
                return put_name_flag(s, host, flag)
            else:
                print("Invalid vuln", file=sys.stderr)
                return Result(ERROR, "Wrong command", "Invalid vuln: " + str(vuln))
    except RestException as re:
        print(re, file=sys.stderr)
        return Result(MUMBLE, 'Service API Error', str(re))    
    except requests.exceptions.ConnectionError as ce:
        print(ce, file=sys.stderr)
        return Result(DOWN, "Service is down", str(ce))
    except:
        print('unknown err', exc_info())
        return Result(ERROR, "Unknown error", traceback_to_string())


def put_name_flag(s, host, flag):
    login, password = getCred(flag)
    print(login, password, host)

    #fake registration
    #sfake = requests.Session()
    #fake = Faker()
    #flogin, fpassword =  fake.name().split()
    #flogin = md5((flogin).encode('utf-8')).hexdigest()[:8]
    #fakeflag = fake.phone_number()
    #client.registration(sfake, flogin, fpassword, host)
    #client.put_name(sfake, host, fakeflag)
    
    #main work    
    client.registration(s, login, password, host)
    #client.login(s, login, password, host)
    client.put_name(s, host, flag)

    return Result(SUCCESS, 'Flag is placed', 'vuln ' + str(settings.PHP_VULN))



