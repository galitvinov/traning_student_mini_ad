from settings import SERVICE_PORT

BASE_URL = 'http://%s:%s/'

url_map = {
    'registration': BASE_URL + 'registration',
    'register': BASE_URL + 'register',
    'profile': BASE_URL + 'profile',
    'catalog': BASE_URL + 'catalog',
    'login': BASE_URL + 'login',
    'search': BASE_URL + 'search',
    'catalog_add': BASE_URL + 'catalog/add',
    'about': BASE_URL + 'about',
    'home': BASE_URL,
}

def get_url(host, endpoint):
    return url_map[endpoint] % (host, SERVICE_PORT)
