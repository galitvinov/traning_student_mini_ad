from settings import DEBUG
from utils import RestException

def print_request(method, url, headers, body=None):
    if DEBUG:
        print()
        print(method + ' ' + url)
        print(headers)
        if body:
            print(body)


def handle_response(response, label):
    if response.status_code != 200:
        raise RestException("%s failed: %s" % (label, response.status_code))
    elif DEBUG:
        print('%s succeed: %s' % (label, response.status_code))
