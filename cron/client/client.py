from sys import stderr
import UserAgents
import json
from urllib.parse import urlencode
from utils import RestException
from settings import (
    TIMEOUT
)
from client.api_urls import (
    get_url,
)
from client.utils import (
    print_request,
    handle_response,
)
import random
from hashlib import md5
from bs4 import BeautifulSoup
from faker import Faker

def registration(session, user, password, addr):
    url = get_url(addr, 'register')
    ua = UserAgents.get()
    headers = {
        'User-Agent': ua,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
#    response = session.get(url, headers=headers, timeout=TIMEOUT)
#    print_request('GET', response.url, headers)
#    handle_response(response, 'Register')
    
    url = get_url(addr, 'registration')
    email = user + random.choice(['@mail.ru','@gmail.com','@yandex.ru','@outlook.com'])
    creds = urlencode({"name": user, "password": password, "email": email})
    response = session.post(url, headers=headers, data=creds, allow_redirects=True, timeout=TIMEOUT)
    print_request('POST', response.url, headers, creds)
    handle_response(response, 'Registration')


def login(session, user, password, addr):
    url = get_url(addr, 'login')
    ua = UserAgents.get()
    headers = {
        'User-Agent': ua,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    
    creds = urlencode({"name": user, "password": password})
    response = session.post(url, headers=headers, data=creds, timeout=TIMEOUT)
    if "loginCookie" not in session.cookies.get_dict():
        raise RestException("Login failed: 401")
    print_request('POST', response.url, headers, creds)
    handle_response(response, 'Login')



def put_name(session, addr, flag):
    url = get_url(addr, 'catalog_add')   
    ua = UserAgents.get()
#    headers = {
#        'User-Agent': ua,
#        'Content-Type': 'application/x-www-form-urlencoded'
#    }
    
    fake = Faker()
    info = fake.text()
    #name_p = md5(("nickname"+flag).encode('utf-8')).hexdigest()[:8]
    name_p = fake.text(max_nb_chars=20)

    pic_num=random.randint(1,20)
    files = {'filePhoto': open('pic/ (' + str(pic_num) + ').jpg', 'rb')}
    data = {"name": name_p, "info": info, "code": flag, "price": "20", "access": random.choice(["on", "on", "on", "off"])}
    response = session.post(url, data = data, files=files, timeout=TIMEOUT)
    print_request('POST', response.url, data)
    handle_response(response, 'Create product')


def get_name(session, addr, flag):
    url = get_url(addr, 'profile')
    ua = UserAgents.get()
    headers = {
        'User-Agent': ua,
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    
    response = session.get(url, headers=headers, timeout=TIMEOUT)
    print_request('GET', response.url, headers)
    handle_response(response, 'Get Profile')

    url = ''
    name_p = md5(("nickname"+flag).encode('utf-8')).hexdigest()[:8]
    soup = BeautifulSoup(response.content, "html.parser")
    for link in soup.findAll('a'):
        if name_p in link.text:
            url = get_url(addr, 'home')[:-1] + link.get('href')
    if(url == ''):
        return ''
        
    response = session.get(url, headers=headers, timeout=TIMEOUT)
    print_request('GET', response.url, headers)
    handle_response(response, 'Get Product')
    
    content = response.content.decode("UTF-8")
    return content
    
    
