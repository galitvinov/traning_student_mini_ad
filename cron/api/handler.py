from methods.info import info
from methods.health_check import health_check
from methods.put_flag import put_flag
from methods.get_flag import get_flag
from methods.result import (
    ERROR,
    SUCCESS,
    CORRUPT,
    MUMBLE,
    DOWN,
)

def http_code_by_status(status):
    return {
        SUCCESS: 200,
        CORRUPT: 404,
        ERROR: 500,
        MUMBLE: 502,
        DOWN: 503,
    }[status]


def info_get():
    result = info()
    return result.extra_data, 200


def flag_post(body):
    result = put_flag(
        host=body['host'],
        flag=body['flag'],
        vuln=body['vuln'],
    )

#    if result.status == SUCCESS:
#        return {
 #           'state': result.extra_data,
  #      }, 200
    print(result.status)
    if result.status == SUCCESS:
        return result.message, 200
    return {
        'code': result.status,
        'debug': result.debug,
        'message': result.message,
    }, http_code_by_status(result.status)


def flag_put(body):
    result = get_flag(
        host=body['host'],
        uid=body['state'],
        flag=body['flag'],
        vuln=body['vuln'],
    )
    
    if result.status == SUCCESS:
        return result.message, 200

    return {
        'code': result.status,
        'debug': result.debug,
        'message': result.message,
    }, http_code_by_status(result.status)


def hosts_get(host):
    result = health_check(host)
    if result.status == SUCCESS:
        return result.message, 200

    return {
        'code': result.status,
        'debug': result.debug,
        'message': result.message,
    }, http_code_by_status(result.status)
