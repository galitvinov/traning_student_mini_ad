from datetime import datetime
import time
import subprocess
import random
import string
import mysql.connector
import os

def generate_random_string(length):
    characters = string.ascii_uppercase + string.digits
    rand_string = ''.join(random.choice(characters) for i in range(length))
    return rand_string

time.sleep(10)
i = 0
while True:
    now = datetime.now()
    if i == 2:
        gen_flag = generate_random_string(31)+"="
        os.system("python3 script.py put app-service {} 1".format(gen_flag))
        mydb = mysql.connector.connect(
          host = "scoredb",
          user='connecter',
          password='connecter_pass',
          database='MDB'
        )
        cursor = mydb.cursor()
        cursor.execute("INSERT INTO Flags (flag,time) VALUES ('{}', NOW());".format(gen_flag))
        mydb.commit()
        i = 0
        print(now.strftime("%H:%M:%S"), gen_flag, ' FLAG!!!')
    else:
        gen_flag = generate_random_string(10)+"@__EMPTY__@"+generate_random_string(10)
        os.system("python3 script.py put app-service {} 1".format(gen_flag))
        i = i + 1
        print(now.strftime("%H:%M:%S"), gen_flag)
    time.sleep(20)
