import random

UserAgents = None

def get():
	global UserAgents
	if UserAgents is None:
#		with open('/someservice/checker/user-agents') as fin:
		with open('user-agents') as fin:
			UserAgents = [line.strip() for line in fin]
	return random.choice(UserAgents)
