from flask import Flask, request, jsonify, render_template
import mysql.connector


app = Flask(__name__, template_folder='.')

def get_db_connection():
    return mysql.connector.connect(
        host="scoredb",
        user='connecter',
        password='connecter_pass',
        database='MDB'
    )

@app.route('/')
@app.route('/index')
def index():
    mydb = get_db_connection()
    cursor = mydb.cursor()
    cursor.execute("SELECT id, login, (SELECT COUNT(*) FROM Send_flags sf WHERE sf.id_usr = u.id) AS 'rank' FROM User u ORDER BY (SELECT COUNT(*) FROM Send_flags sf WHERE sf.id_usr = u.id) DESC, id ASC;")
    users=cursor.fetchall()
    cursor.close()
    mydb.close()

    return render_template('index.html', players=users)

@app.route('/flags', methods=['PUT'])
def update_flags():
    mydb = get_db_connection()
    cursor = mydb.cursor()
    
    token = request.headers.get('X-Team-Token')
    cursor.execute("SELECT id FROM User WHERE auth=%s;", (token,))
    login_r = cursor.fetchone()
    if login_r is None:
        return jsonify({"error": "Authorization failed!"}), 401
    login_r = login_r[0]

    flags = request.get_json(force=True)
    
    if not isinstance(flags, list):
        return jsonify({"error": "Invalid flags format"}), 400
    
    answer = []

    for flag in flags:
        cursor.execute("SELECT id FROM Flags WHERE flag=%s;", (flag,))
        flag_r = cursor.fetchone()
        if flag_r is None:
            answer.append({flag: "Flag does not exist!"})
            continue
        flag_r = flag_r[0]
        
        cursor.execute("SELECT id, time FROM Send_flags WHERE id_usr={} AND id_flag={};".format(login_r, flag_r))
        find_flag = cursor.fetchone()
        if find_flag is not None:
            answer.append({flag: "This flag has already been surrendered!"})
            continue
        
        cursor.execute("INSERT INTO Send_flags (id_usr, id_flag, time) VALUES ({},{},NOW());".format(login_r, flag_r))
        mydb.commit()
        answer.append({flag: "Flag accepted!"})

    cursor.close()
    mydb.close()

    return jsonify({"flags": answer}), 200


if __name__=="__main__":
    app.run(host='0.0.0.0',port="8000", debug=True)
