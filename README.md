# Сервис для тренеровки студентов.
Система позволяет получить навыки для автоматизации запросов и поиску уязвимостей для соревнований по информационной безопасности.

## 1.Установка дополнительный пакетов
```bash
apt-get update
apt-get install git mc docker.io docker-compose
```
## 2.Клонирование репозитория
```bash
git clone https://gitlab.com/galitvinov/traning_student_mini_ad.git
cd traning_student_mini_ad/
```
## 3.Запуск и проверка работоспособности
```bash
docker-compose up --build -d
docker ps
```